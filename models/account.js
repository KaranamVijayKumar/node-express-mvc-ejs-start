/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Prajakt Uttamrao Khawase <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountID: { type: Number, required: true, unique: true, default: 555 },
  email: { type: String, required: true },
  dateCreated: { type: Date, required: true, default: Date.now() },

  accountType: { type: String, enum: ['NA', 'savings account', 'checkings account', 'current account'], required: true, default: 'NA' },
  paid: { type: Boolean, default: false }
})

module.exports = mongoose.model('Account', AccountSchema)
