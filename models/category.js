/** 
*  Category model
*  Describes the characteristics of each attribute in a category resource.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  categoryId: { type: Number, required: true, unique: true },
  categoryName: { type: String, required: false, default: 'description' },
  categoryLimit: { type: Number, required: true }
})

module.exports = mongoose.model('category', CategorySchema)
