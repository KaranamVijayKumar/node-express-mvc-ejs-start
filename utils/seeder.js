// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const customers = require('../data/customers.json')
const category = require('../data/cateogry.json')
const orders = require('../data/orders.json')
const transaction = require('../data/transaction.json')
const puppies = require('../data/puppies.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // users don't depend on anything else...................

  db.users = new Datastore()
  db.users.loadDatabase()

  // insert the sample data into our data store
  db.users.insert(users)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users seeded`)

  // Cateogry don't depend on anything else .....................

  db.category = new Datastore()
  db.category.loadDatabase()

  // insert the sample data into our data store
  db.category.insert(category)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.category = db.category.find(category)
  LOG.debug(`${app.locals.category.query.length} category seeded`)

  // accounts need a customer beforehand .................................

  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  // insert the sample data into our data store
  db.accounts.insert(accounts)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)

  // Each Order Line Item needs a category and an order beforehand ...................

  db.accountLineItems = new Datastore()
  db.accountLineItems.loadDatabase()

  // insert the sample data into our data store
  db.accountLineItems.insert(accountLineItems)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transaction = db.transaction.find(transaction)
  LOG.debug(`${app.locals.transaction.query.length} transaction seeded`)
  LOG.info('END Seeder.Sample data read and verified.')

}
