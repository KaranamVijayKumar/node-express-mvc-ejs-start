/** 
*  Transaction model
*  Describes the characteristics of each attribute in an transaction - one entry on a user's transaction.
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionID: { type: Number, required: true },
  accountNumber: { type: Number, required: true },
  transactionamount: { type: Number, required: true, default: 1 }
})

module.exports = mongoose.model('OrderLineItem', TransactionSchema)
